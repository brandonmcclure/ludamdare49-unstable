using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Edge : MonoBehaviour
{
	Vector2 startPos;
	Vector2 endpos;
	Vector3 mousePos;
	Vector3 mouseDir;
	public Camera cam;
	public LineRenderer lr;
	public Text debugText;

	bool buildMode = false;
    // Start is called before the first frame update
    void Start()
    {
		buildMode = false;
		lr.SetPosition(0, new Vector2(2,2));
		lr.SetPosition(1, new Vector2(2, 2));


	}

	private void AddPhysics2DRaycaster()
	{
		Physics2DRaycaster physicsRaycaster = FindObjectOfType<Physics2DRaycaster>();
		if (physicsRaycaster == null)
		{
			Camera.main.gameObject.AddComponent<Physics2DRaycaster>();
		}
	}
	// Update is called once per frame
	void Update()
    {
		mousePos = cam.ScreenToWorldPoint(Input.mousePosition);
		var debugMsg = "mousePosition: " + mousePos.x.ToString() + "," + mousePos.y.ToString();
		debugMsg += "buildMode: " + buildMode;
		debugText.text = debugMsg;

		if (Input.GetMouseButtonDown(0) && !buildMode)
		{
			Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);
			mouseDir = mousePos - gameObject.transform.localPosition;
			mouseDir.z = 0;
			mouseDir = mouseDir.normalized;

			RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
			if (hit.collider != null)
			{
				Debug.Log(hit.collider.gameObject.name);
				EnterBuildMode();
			}
		}
		if (Input.GetMouseButtonDown(0) && buildMode)
		{

			RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);
			if (hit.collider != null)
			{
				var go = hit.collider.gameObject;
				if (go.CompareTag("Node"))
				{
					lr.SetPosition(1, mousePos);
					ExitBuildMode();
				}
			}
			
		}
		
		if(buildMode){
			lr.SetPosition(1, mousePos);

		}
		
    }

	void EnterBuildMode(){
		buildMode =true;
			lr.enabled =true;
		lr.SetPosition(1, mousePos);
	}
	void ExitBuildMode(){

		buildMode =false;
	}
}
