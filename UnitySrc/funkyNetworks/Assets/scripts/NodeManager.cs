using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NodeManager : MonoBehaviour
{
    public GameObject Canvas;
    public GameObject Node;
	public Camera MainCam;
    bool canplace;
    bool releasedbutton;
    Vector3 mousePos;
	int maxActiveNodes = 0;
	int activeNodes =0;


    private int _score;
    public int score { get { return _score; } private set { _score = value; } }

    public delegate void OnScoreIncreaseDelegate();
    public event OnScoreIncreaseDelegate scoreIncreaseDelegate;

    public void Start()
    {
        releasedbutton = true;

        canplace = false;
    }

    void OnDisable()
    {
        DelegateHandler.scoreIncreaseDelegate -= IncreaseScore;
    }

    public void IncreaseScore()
    {
        score++;
        scoreIncreaseDelegate();
    }
	private void Update()
    {
        if(Input.GetMouseButtonDown(0))
        {
            releasedbutton = false;
            canplace = true;
            mousePos = Input.mousePosition;
        }
        if(Input.GetMouseButtonUp(0))
        {
            releasedbutton = true;
            canplace = false;
        }

           
        if(releasedbutton == false && canplace && activeNodes < maxActiveNodes)
        {
			Vector3 point = MainCam.ScreenToWorldPoint(new Vector3(mousePos.x, mousePos.y, MainCam.nearClipPlane));

            GameObject tmpObj = Instantiate(Node,point, Quaternion.identity);
SpriteRenderer spriteRenderer = tmpObj.GetComponent<SpriteRenderer>();

 float cameraHeight = Camera.main.orthographicSize * 2;
    Vector2 cameraSize = new Vector2(Camera.main.aspect * cameraHeight, cameraHeight);
    Vector2 spriteSize = spriteRenderer.sprite.bounds.size;   

	Vector2 scale = new Vector2(40,40);


transform.position = Vector2.zero; // Optional
transform.localScale = scale;
activeNodes++;

canplace = false;

            

        }

    }
}
